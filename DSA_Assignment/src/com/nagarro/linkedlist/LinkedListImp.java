package com.nagarro.linkedlist;

import java.util.Scanner;

public class LinkedListImp {
	static LinkedListImp head = null;
	int data;
	LinkedListImp next;

	LinkedListImp() {
	}

	LinkedListImp(int d) {
		data = d;
		next = null;
	}

	public void insert() {
		Scanner sc = new Scanner(System.in);
		System.out.println("enter data");
		int data = sc.nextInt();
		LinkedListImp listimp = new LinkedListImp(data);
		LinkedListImp newnode = listimp;
		if (listimp.head == null) {
			listimp.head = newnode;
		} else {
			LinkedListImp temp = listimp.head;
			while (temp.next != null) {
				temp = temp.next;
			}
			temp.next = newnode;
		}
		newnode.next = null;

	}

	public void traverse() {

		LinkedListImp tvrs = head;
		while (tvrs != null) {
			System.out.print(tvrs.data + " ->");
			tvrs = tvrs.next;

		}

		System.out.println();
	}

	public void delete() {

		LinkedListImp temp = head;

		while (temp.next.next != null) {
			temp = temp.next;
		}

		temp.next = null;
	}

	public void insert_at_position() {
		Scanner sc = new Scanner(System.in);
		System.out.println("enter any position");
		int num = sc.nextInt();
		int n = 1;
		System.out.println("enter data");
		int data = sc.nextInt();
		LinkedListImp listimp = new LinkedListImp(data);
		LinkedListImp newnode = listimp;
		LinkedListImp temp = head;
		LinkedListImp temp1;
		if (num == 1) {
			newnode.next = temp;
			head = newnode;
		}
		while (n++ < num - 1) {
			temp = temp.next;
		}
		temp1 = temp.next;
		temp.next = newnode;
		newnode.next = temp1;
	}

	public int size() {
		int size = 0;

		LinkedListImp tvrs = head;
		while (tvrs != null) {
			size++;
			tvrs = tvrs.next;
		}
		System.out.println("size is :" + size);
		return size;

	}

	public void delete_at_position() {
		Scanner sc = new Scanner(System.in);
		System.out.println("enter any position");
		int pos = sc.nextInt();
		LinkedListImp copy = head;
		int n = 1;
		while (n++ < pos - 1) {
			copy = copy.next;
		}
		copy.next = copy.next.next;

	}

	public void center() {
		int size = size();
		size = size / 2;

		LinkedListImp tvrs = head;
		int k = 0;
		while (k++ < size) {
			tvrs = tvrs.next;
		}
		System.out.println("center is :" + tvrs.data);

	}

	public void reverse() {

		LinkedListImp temp, current = null, temp1;
		temp = head;
		while (temp.next != null) {
			temp1 = temp.next;
			temp.next = current;
			current = temp;
			temp = temp1;

		}
		temp.next = current;
		head = temp;
	}

	public void sort() {

		LinkedListImp temp = head;
		LinkedListImp current, temp1;
		int store;
		for (current = temp; current != null; current = current.next) {
			for (temp1 = current.next; temp1 != null; temp1 = temp1.next) {
				if (current.data > temp1.data) {
					store = current.data;
					current.data = temp1.data;
					temp1.data = store;
				}
			}
		}

		traverse();

	}
}
