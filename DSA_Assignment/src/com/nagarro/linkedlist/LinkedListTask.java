package com.nagarro.linkedlist;

import java.util.Scanner;

public class LinkedListTask {
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);
		LinkedListImp list = new LinkedListImp();
		int choice;
		while (true) {
			System.out.println("enter any choice");
			System.out.println("1 for insert");
			System.out.println("2 for insert at position");
			System.out.println("3 for delete");
			System.out.println("4 for size");
			System.out.println("5 for delete at position");
			System.out.println("6 for traverse/display");
			System.out.println("7 for center");
			System.out.println("8 for reverse");
			System.out.println("9 for sort");
			choice = sc.nextInt();
			switch (choice) {
			case 1:
				list.insert();
				break;
			case 2:
				list.insert_at_position();
				break;
			case 3:
				list.delete();
				break;
			case 4:
				list.size();
				break;
			case 5:
				list.delete_at_position();
				break;
			case 6:
				list.traverse();
				break;
			case 7:
				list.center();
				break;
			case 8:
				list.reverse();
				break;
			case 9:
				list.sort();
			default:
				System.out.println("please enter right value");

			}
		}

	}
}
