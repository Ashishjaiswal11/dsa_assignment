package com.nagarro.hashtable;

public class HashTable {
	public static void main(String[] args) {
		HashTableImp h1 = new HashTableImp();
		h1.put("rk", "cc");
		h1.put("rk2", "aa");
		h1.put("rk3", "bb");
		System.out.println("value " + h1.get("rk2"));
		h1.remove("rk3");
		System.out.println("value " + h1.get("rk3"));
		System.out.println("size " + h1.size());
		System.out.println("size " + h1.contains("bb"));
		h1.printElement();
	}
}
