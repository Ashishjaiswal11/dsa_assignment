package com.nagarro.queue;

public class QueueImp {
	int queue[] = new int[5];
	int size = 5, front;
	int rear = -1;

	public void enQueue(int data) {
		if (!isFull()) {
			rear++;
			queue[rear] = data;
		} else {
			System.out.println("Queue is full");
		}

	}

	public void cointains() {
		for (int i = 0; i <= rear; i++) {
			System.out.println("The elements of the queue is " + queue[i]);
		}
	}

	public int deQueue() {
		if (!isEmpty()) {
			int data = queue[front];
			front++;
			return data;
		} else {
			System.out.println("Queue id empty");
			return 0;
		}
	}

	public boolean isEmpty() {
		return rear == -1;
	}

	public boolean isFull() {
		return rear == size - 1;
	}

	public int peek() {
		if (isEmpty()) {
			System.out.println("Queue is empty");
			return 0;
		} else {
			return queue[front];
		}
	}

	public int size() {
		return rear + 1;
	}

	public void center() {
		System.out.println("The center element is " + queue[size() / 2]);
	}

	public void traverse() {
		if (!isEmpty()) {
			for (int i = size() - 1; i >= 0; i--) {
				System.out.println("The elements of the queue is " + queue[i]);

			}
		}
	}

	public void reverse() {
		QueueTask temp = new QueueTask();
		while (!isEmpty()) {
			enQueue(queue[rear]);
			deQueue();
		}
		rear = 0;
		front = 1;
		for (int i = size() - 1; i >= 0; i--) {
			enQueue(queue[i]);
		}
	}
}
